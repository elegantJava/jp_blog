package service;


import com.zjp.blog.model.Tag;
import com.zjp.blog.service.TagService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * @author pj_zhong
 * @email pj_zhong@163.com / pj_zhong@gmail.com
 */
@ContextConfiguration("classpath:spring/application.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class TagServiceTest
{
    @Autowired
    TagService tagService;

    @Test
    public void test()
    {
         List<Integer> list  = new ArrayList<Integer>();

            list.add(50);
        list.add(51);

        for(Tag t : tagService.getByIds(list)){
            System.out.println(t);
        }
    }
}
