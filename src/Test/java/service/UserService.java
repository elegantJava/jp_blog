package service;

import com.zjp.blog.client.UserMapper;
import com.zjp.blog.model.User;
import com.zjp.blog.utils.MD5;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

/**
 * @author pj_zhong
 * @email pj_zhong@163.com / pj_zhong@gmail.com
 */

@ContextConfiguration("classpath:spring/application.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class UserService
{
    @Autowired
    private UserMapper mapper;

   public User selectByEmail(String email)
    {
        return mapper.selectByEmail(email);
    }

    @Test
    public void test()
    {
        User user = mapper.selectByEmail("pj_zhong@163.com");

        System.out.println(user.getEmail());
    }

    public User createUser(User user)
    {
        user.setPassword(MD5.md5(user.getPassword()));
        user.setCreateTime(new Date());
        user.setUpdateTime(user.getUpdateTime());
        mapper.insert(user);
        return  user;
    }

    public int  updateById(User user)
    {
        user.setUpdateTime(new Date());
        return  mapper.updateByPrimaryKeySelective(user);
    }

}
