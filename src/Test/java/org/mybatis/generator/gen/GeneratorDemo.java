package org.mybatis.generator.gen;

import org.junit.Test;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.exception.InvalidConfigurationException;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *GeneratorDemo
 */
public class GeneratorDemo {
    @Test
    public void genSystem() throws Exception {

/*        File file = new File("genConfig/generatorConfig-1.1.xml");

        if(file.exists())
        {
            System.out.println("great");
        }*/
        generateFiles("genConfig/generatorConfig-1.1.xml");
    }

    private void generateFiles(String configResource) throws Exception
    {
        List<String> warnings = new ArrayList<String>();
        ConfigurationParser cp = new ConfigurationParser(warnings);
          Configuration config = cp.parseConfiguration(GeneratorDemo.class.getClassLoader()
                .getResourceAsStream(configResource));
            DefaultShellCallback shellCallback = new DefaultShellCallback(true);

        try {
            MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, shellCallback, warnings);
            myBatisGenerator.generate(null);
        } catch (InvalidConfigurationException e) {
            throw e;
        }
        for (String warning : warnings) {
            System.out.println(warning);
        }
    }
}
