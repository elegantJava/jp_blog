<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${staticpath}/bootstrap/css/bootstrap.min.css">
</head>
<body>
   <div class="container" style="max-width:50%;">
        <div class="page-header">
            <h1>404 NotFound</h1>
        </div>
       <p class="lead">
           Sorry,The page you looking for is hiding in some place we don't know :(<br>
           But,we will find it!
       </p>
       <p><a href="${basepath}/index">Click here to return home</a></p>
   </div>
</body>
</html>