<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${staticpath}/bootstrap/css/bootstrap.min.css">
</head>
<body>
<div class="container" style="max-width:50%;">
    <div class="page-header">
        <h1>500 InterError</h1>
    </div>
    <p class="lead">
        Sorry,Something is happening in the service... :(<br>
    </p>
    <p><a href="${basepath}/index">Click here to return home</a></p>
</div>
</body>
</html>