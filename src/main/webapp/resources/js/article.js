/*this file has some code is similar to the file main.js  */


var load   =  '../article/load?';

$(function(){
    $("#tag").chosen({
        placeholder_text_multiple:"Search By Tags",
        no_results_text:"Not Find",
        allow_single_deselect:true
    });

    loadList(load);
})

function toPage(PageNum) {
    var pages = $('#pages').val();
    var param = buildParam();
    var paging = load;

    if(PageNum >0 && PageNum <=pages){
        paging = paging + "pageNum=" + PageNum;
    }

    paging = paging + "&param=" + param;

    loadList(paging);
}

function loadList(url) {
    console.info(url);
    $.ajax({
        url :url,
        success: function(data) {
            $('#dataList').html(data);
        }
    });
}

function buildParam() {
    var param = {};

    var keyword = $('#keyword').val();
    if( !isEmpty(keyword) ) {
        param["title"] = keyword
    }

    var categoryId = $("#categoryId").val();
    if(!isEmpty(categoryId) && categoryId != '-1'){
        param["categoryId"] = categoryId;
    }

    var tagIds = $('#tag').val();
    if(tagIds != null){
        param["tagIds"]=tagIds;
    }

    return JSON.stringify(param);
}

function deletePost(postId,posTitle) {
    if(confirm("Do you want to Delete @"+posTitle))
    {
        $('#postID').val(postId);
        $('#deleteById').submit();
    }
}

function isEmpty(str){
    if (!str || $.trim(str).length <= 0)
        return true;
    return false;
}