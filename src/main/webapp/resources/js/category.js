var load   =  '../category/load';
var update =  '../category/update';

$(function(){
    loadList(load);
})

function loadList(url) {
    $.ajax({
        url :url,
        success: function(data) {
            $('#dataList').html(data);
        }
    });
}

function toPage(pageNum){
    var pages = $('#pages').val();
    var keyword =  $('#keyword').val();

    if(pageNum <=0 || pageNum> pages){
        return false;
    }

    var paging  = load + '?pageNum=' + pageNum + '&keyword=' + keyword;
    loadList(paging);
}

function Delete(Id,Name) {
    if(confirm("Do you want to Delete @" + Name))
    {
        $('#id').val(Id);
        console.info($('#id').val())
        $('#delete').submit();
    }
}

function addCategory(id){
    var url = update;
    if(id>0) {
        url = url + "?id=" + id;
    }

    $.ajax({
        url :url,
        success  : function(data)
        {
            $('#addContent').html(data);
            $('#addModal').modal('show');
        }
    });
}