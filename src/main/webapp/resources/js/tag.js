var url= '../tags/load';
$(function(){
    //load data
    loadList(url);
})

function loadList(url) {
    $.ajax({
        url :url,
        success: function(data) {
            $('#dataList').html(data);
        }
    });
}

function toPage(pageNum){
    var pages = $('#pages').val();
    var keyword =  $('#keyword').val();


    if(pageNum <=0 || pageNum> pages){
        return false;
    }

    var paging  = url + '?pageNum=' + pageNum + '&keyword=' + keyword;
    loadList(paging);
}

function deleteTag(tagId,tagName) {
    if(confirm("Do you want to Delete @"+tagName)) {
        $('#tagId').val(tagId);
        $('#delete').submit();
    }
}

function addTag(tagId){
    var url = '../tags/update';

    /*Editing tag if tagId great than 0*/
    if(tagId>0) {
        url = url + "?id=" + tagId;
    }

    $.ajax({
        url :url,
        success  : function(data)
        {
            $('#addTagContent').html(data);
            $('#addTagModal').modal('show');
        }
    });
}