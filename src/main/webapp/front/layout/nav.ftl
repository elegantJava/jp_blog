<#macro nav>
<nav id="navbar" class="navbar navbar-default  navbar-fixed-top navbar-burger" role="navigation">
    <!-- if you want to keep the navbar hidden you can add this class to the navbar "navbar-burger"-->
    <div class="container">
        <div class="navbar-header">
            <button id="menu-toggle" type="button" class="navbar-toggle" style="color: #0f0f0f" data-toggle="collapse" data-target="#example">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar bar1"></span>
                <span class="icon-bar bar2"></span>
                <span class="icon-bar bar3"></span>
            </button>
            <a  href="${basepath}/index" class="navbar-brand" >
                        PJ_ZHONG
            </a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right navbar-uppercase">
                <li>
                    <a data-scroll="true" class="text-center">
                         <p><img src="${staticpath}/images/me.jpg" class="img-rounded"  style="width: 120px;height: 150px;" alt=""></p>
                         <p>Great to see you!</p>
                    </a>
                </li>
                <li>
                    <a href="${basepath}/index" data-scroll="true" data-id="#whoWeAre">
                        Home
                    </a>
                </li>
                <li>
                    <a data-scroll="true" data-id="#whoWeAre">
                        About
                    </a>
                </li>
                <li>
                    <a data-scroll="true" data-id="#workflow">
                        Creating
                    </a>
                </li>
                <li>
                    <a data-scroll="true" data-id="#workflow">
                        Creating
                    </a>
                </li>
                <li>
                    <a data-scroll="true" data-id="#workflow">
                        Creating
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
</#macro>