<#import "nav.ftl" as nav>
<#import "/manage/layout/footer.ftl" as foot/>
<#macro htmlbase>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>pj_zhong</title>
    <link rel="stylesheet" type="text/css" href="${staticpath}/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/css/rubick_pres.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/css/theme-light.css">
    <script type="text/javascript" src="${staticpath}/jquery.min.js"></script>
</head>
<body class="bodyColor">
  　<@nav.nav/>
    <#nested/>
    <@foot.footer/>
</body>
<script type="text/javascript" src="${staticpath}/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${staticpath}/js/modernizr.js"></script>
<script type="text/javascript" src="${staticpath}/js/rubick_pres.js"></script>
</html>
</#macro>