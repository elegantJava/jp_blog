<#import "/front/layout/html_base.ftl" as html/>
<@html.htmlbase>
<script type="text/javascript" src="${staticpath}/js/main.js"></script>
<div class="container white">
    <div class="row">
       <div class="col-lg-2">
           <#if categories??>
               <ul class="list-group">

                  <h3> <a href="javascript:" onclick="toIndex()"><i class="fa fa-flag" aria-hidden="true"></i>&nbsp;Categories</a></h3>

                   <#list categories as c>
                       <li class="list-group-item" id="c_${c.getId()}" onclick="toCategory(${c.getId()})">
                           <a >${c.getCategoryName()}</a>
                       </li>
                   </#list>
               </ul>
           </#if>
       </div>

       <div class="col-lg-7 article-list"></div>

        <div class="col-lg-3 tag_wrapper">
            <span class="input-group">
                <input id="keyword" class="form-control" type="text"  name="title" placeholder="Search articles">
                <span class="input-group-btn">
                    <button class="btn  btn-primary" onclick="search()">Search</button>
                </span>
            </span>
             <#if tags??>
                   <h3 class="title">
                       <strong>
                           <a class="tag">
                               <i class="fa fa-tags" aria-hidden="true"></i>Tags:
                           </a>
                       </strong>
                   </h3>
                   <ul class="tags_text">
                        <#list tags as t>
                            <li>
                                <a href="javascript:" id="t_${t.getId()}"onclick="toTag(${t.getId()})">${t.getName()}</a>
                            </li>
                        </#list>
                   </ul>
             </#if>
        </div>

    </div>
</div>
</@html.htmlbase>