<#import "/front/layout/html_base.ftl" as html/>
<@html.htmlbase>
<div class="container white" >
    <div class="row" style="text-align: center;">
        <div class="col-sm-12">
            <h2 class="blog-title">${article.title}</h2>
        </div>

        <p class="col-sm-12">
            <a class="btn btn-xs btn-default"><i class="fa fa-flag" aria-hidden="true"></i> Category:${article.getCategoryName()!""}</a>&nbsp;
            <a class="btn btn-xs btn-default"> <i class="fa fa-user" aria-hidden="true"></i> Author:${article.getAuthor()}</a> &nbsp;
            <a class="btn btn-xs btn-default"><i class="fa fa-calendar" aria-hidden="true"></i> Update at:${article.updateTime?date!''}</a>
        </p>
    </div>
    <div class="row" style="padding-left: 15%;padding-right: 15%" >
        ${article.content}
        <#if article.getTags()??>
            Tags:
                <#list article.getTags() as tag>
                    <a  class="btn btn-xs btn-default"  >${tag.name}</a>
                </#list>
        </#if>
    </div>
    <#include "comment.html">
</div>
</@html.htmlbase>
