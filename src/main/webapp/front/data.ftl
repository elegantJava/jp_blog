
<h3 class="title">
    <strong>
        <a >Articles</a>
        <#if category??>
          <a class="category">
              <i class="fa fa-flag" aria-hidden="true"></i>
             ${category.getCategoryName()}
          </a>
        </#if>

        <#if tags??>
            <a class="tag">
                <i class="fa fa-tag" aria-hidden="true"></i>
                <#list tags as t>
                     ${t.getName()},
                </#list>
            </a>
        </#if>
    </strong>
</h3>

<#if articles??>
    <#list articles as a>
        <div class="article">
            <header>
                <h3>
                    <i class="label  label-info">${a.getCategoryName()!""}</i>
                    <b><a href="${basepath}/article/${a.getId()}" target="_blank">${a.getTitle()}</a></b>
                </h3>
            </header>

            <p class="text-muted detail">
                <i class="fa fa-user" aria-hidden="true"></i> Author:${a.getAuthor()}
                &nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-calendar" aria-hidden="true"></i> Published on:${a.getUpdateTime()?date!""}
            </p>

            <div class="preview">
                ${a.getPreview()}</i></strong></pre></b>
            </div>

            <p class="text-muted detail">
                <i class="fa fa-eye"></i> Views:${a.getReadingCount()} &nbsp;&nbsp;&nbsp;&nbsp;
                <#if a.getTags()??>
                    Tag(s):
                    <#list a.getTags() as t>
                          <a  class="btn btn-xs btn-default"> ${t.getName()}</a>
                    </#list>
                </#if>
            </p>
        </div>
    </#list>
<#else>
    <h3><a  class="text-muted">Sorry,No articles here :)</a></h3>
</#if>

<#include "/manage/layout/pager.ftl"/>