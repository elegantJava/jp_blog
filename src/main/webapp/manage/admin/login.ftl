<#import "/manage/layout/html_base.ftl" as  html />
<@html.htmlbase>
<form class="form-signin" action="${basepath}/manage/login"  method="post" >
  <h2 class="form-signin-heading">Please Login</h2>

  <label for="inputEmail" class="sr-only">Email address</label>
  <input type="email" name="userEmail" class="form-control" placeholder="Email Address" required autofocus>

  <label for="inputPassword" class="sr-only">Password</label>
  <input type="password" name="password" class="form-control" placeholder="Password"  required>

  <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
</form>
</@html.htmlbase>
