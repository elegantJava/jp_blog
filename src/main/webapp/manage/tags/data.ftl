<#--<#import "/manage/layout/pager.ftl" as  pager />-->
<table id="data" class="table table-stripped" >
    <thead>
    <th>ID</th>
    <th>Name</th>
    <th>Operation</th>
    </thead>
    <tbody >
    <#if items??>
        <#list items as item>
        <tr>
            <td>${item.getId()}</td>
            <td>${item.getName()}</td>
            <td>
                <a class="btn btn-xs btn-primary"  onclick="addTag(${item.getId()})"><i class="fa fa-edit"></i></a>
                <a class="btn btn-xs btn-danger btn-delete" href="javascript:deleteTag(${item.getId()},'${item.getName()}')" ><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        </#list>
    <#else>
        Nothing here,Sorry :(
    </#if>

    </tbody>
</table>

<#include "/manage/layout/pager.ftl"/>