<#import "/manage/layout/html_base.ftl" as  html />
<@html.htmlbase>
<link rel="stylesheet" type="text/css" href="${staticpath}/simditor/styles/simditor.css">
<link rel="stylesheet" type="text/css" href="${staticpath}/chosen/chosen.css">
<div class="container">
    <form class="article-form"  action="${basepath}/manage/article/write" method="post">
        <#if article??>
            <h1>Update <span class="btn btn-primary">${article.title}</span></h1><hr>
            <input type="hidden" name="id" value="${article.id}">
        <#else >
            <h1>New article</h1>
        </#if>

        <!-- Title -->
        <div class="row">
            <div class="col-sm-6">
                <span>Title</span>
                <input class="form-control" id="title" name="title" type="text" style="margin-bottom: 10px;"  value="<#if article??> ${article.title}</#if>" />
            </div>

            <div class="col-sm-6">
                <span>Category</span>
                <select class="form-control" name="categoryId" >
                    <#if categories??>
                        <#list categories as c>
                            <#if article?? && article.getCategoryId() == c.getId()>
                                <option value="${c.getId()}" selected>${c.getCategoryName()}</option>
                            <#else>
                                <option value="${c.getId()}">${c.getCategoryName()}</option>
                            </#if>
                        </#list>
                    </#if>
                </select>
            </div>
        </div>
        <!-- Title End-->

        <!-- Content -->
        <div class="row">
            <div class="col-xs-12">
                <textarea class="form-control" id="content" name="content"  required autofocus><#if article??>${article.content!""}</#if></textarea>
            </div>
        </div>
        <!-- Content End-->

        <div class="row">
            <hr>
            <!-- Author -->
            <div class="col-sm-3">
                <span>Author</span>
                <input class="form-control" type="text" name="author"  value="<#if article??>${article.author}<#else>pj_zhong</#if>"/>
            </div>

            <!-- State -->
            <div class="row">
                <div class="col-sm-3">
                    <span>Status</span>
                    <select class="form-control" name="state" >
                        <#if Status??>
                            <#list Status as s >
                                <#if article?? &&  article.state == s  >
                                    <option value="${s}" selected>${s}</option>
                                <#else>
                                    <option value="${s}">${s}</option>
                                </#if>
                            </#list>
                        </#if>
                    </select>
                </div>

                <!-- Tags -->
                <div class="col-lg-5">
                    <span>Tags</span>
                    <select  class="chosen-select form-control" multiple id="tag" name="tagString"  style="width:450px;" tabindex="5">
                        <#if article?? && article.tags??>
                            <#list article.tags as t>
                                <option value="${t.getId()}" selected>${t.getName()}</option>
                            </#list>
                        </#if>
                        <#if tags??>
                            <#list tags as t >
                                <option value="${t.getId()}">${t.getName()}</option>
                            </#list>
                        </#if>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <hr><button class="btn btn-primary btn-block"  type="submit"<#-- onclick="saveArticle()"-->>
            <#if article??>
                Update article
            <#else>
                Create new article
            </#if>
        </button>
        </div>
    </form>
</div>


<script type="text/javascript" src="${staticpath}/simditor/scripts/js/simditor-all.js"></script>
<script type="text/javascript" src="${staticpath}/chosen/chosen.jquery.js"></script>
<#--<script type="text/javascript" src="${staticpath}/js/article/add_article.js"></script>
<script type="text/javascript" src="${staticpath}/js/validation.js"></script>-->
<script type="text/javascript">
    $(function()
    {
        toolbar = [ 'title', 'bold', 'italic', 'underline', 'strikethrough',
            'color', '|', 'ol', 'ul', 'blockquote', 'code', 'table', '|',
            'link', 'image', 'hr', '|', 'indent', 'outdent' ];
        var editor = new Simditor( {
            textarea : $('#content'),
            placeholder : '这里输入内容...',
            toolbar : toolbar,
            upload : {
                url : '${basepath}/manage/file/upload', //文件上传的接口地址
                params: null, //键值对,指定文件上传接口的额外参数,上传的时候随文件一起提交
                fileKey: 'fileData', //服务器端获取文件数据的参数名
                connectionCount: 3,
                leaveConfirm: '正在上传文件'
            }
        });

        $(".chosen-select").chosen({
            default_multiple_text:"Select some tagIds",
            no_results_text:"Not Find",
            allow_single_deselect:true
        });
    })
</script>
</@html.htmlbase>

