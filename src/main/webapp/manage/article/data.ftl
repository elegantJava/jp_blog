<table class="table table-bordered table-striped">
    <thead>
    <th width="">Title</th>
    <th>Status</th>
    <th>ReadingCount</th>
    <th>Create/UpdateTime</th>
    <th>Operation</th>
    </thead>
    <tbody >
        <#if items??>
            <#list items as item>
            <tr>
                <td>${item.getTitle()}</td>
                <td>${item.getState()}</td>
                <td>${item.getReadingCount()}</td>
                <td>${item.updateTime?string("yyyy-MM-dd HH:MM:SS")!""}</td>
                <td>
                    <a class="btn btn-xs btn-ino" href="${basepath}/article/${item.id}" target="_blank"><i class="fa fa-eye"></i></a>
                    <a class="btn btn-xs btn-primary" href="${basepath}/manage/article/write?id=${item.id}"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-xs btn-danger btn-delete" href="javascript:deletePost(${item.id},'${item.title}')" ><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                ${item.getPreview()}
                </td>
            </tr>
            </#list>
        <#else>
            Nothing here,Sorry :(
        </#if>
    </tbody>
</table>

<#include "/manage/layout/pager.ftl"/>