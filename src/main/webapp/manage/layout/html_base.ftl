<#import "nav.ftl" as nav>
<#import "footer.ftl" as foot>
<#macro htmlbase>
<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>pj_zhong</title>
        <link rel="stylesheet" type="text/css" href="${staticpath}/simditor/styles/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="${staticpath}/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="${staticpath}/css/theme-light.css">

        <script type="text/javascript" src="${staticpath}/jquery.min.js"></script>
        <script type="text/javascript" src="${staticpath}/bootstrap/js/bootstrap.min.js"></script>
</head>
<body >
    <@nav.nav/>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <#if message??>
                    <div class="alert alert-success alert-dismissable fade in" id="alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    ${message}
                    </div>
                </#if>
                <#if warning??>
                    <div class="alert alert-warning alert-dismissable fade in" id="alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    ${warning}
                    </div>
                </#if>
                <#if errorMsg??>
                    <div class="alert alert-danger alert-dismissable fade in" id="alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    ${errorMsg}
                    </div>
                </#if>
            </div>
        </div>
    </div>
    <#nested />
    <@foot.footer/>
</body>
</html>
</#macro>