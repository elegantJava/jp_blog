<#if pager?? && (pager.getPages() > 1)>
<div class="row">
    <input type="hidden" id="pages" value="${pager.getPages()}">
    <ul class="pager">
        <li><a href="javascript:" onclick="toPage(${pager.getPageNum()-1})">Previous</a></li>
        <li class="text-muted">${pager.getPageNum()}/${pager.getPages()}</li>
        <li><a href="javascript:" onclick="toPage(${pager.getPageNum()+1})">Next</a></li>
    </ul>
</div>
</#if>
