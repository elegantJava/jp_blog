<#macro nav>
 <nav class="navbar navbar-default navbar-static-top">
     <div class="container">
         <div class="navbar-header dropdown">
             <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                 <span class="sr-only">Toggle navigation</span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
             </button>
             <a class="navbar-brand dropdown-toggle" href="#" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Project name <span class="caret"></span></a>
             <ul class="dropdown-menu">
                 <li><a href="#">Profile</a></li>
                 <li><a href="#">Setting</a></li>
                 <li role="separator" class="divider"></li>
                 <li><a href="${basepath}/manage/logout">Log Out</a></li>
             </ul>
         </div>
         <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a<#-- href="${basepath}/manage"-->>Home</a></li>
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Category<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="${basepath}/manage/category">Manage Category</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">BLOG<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="${basepath}/manage/article/write">New Blog</a></li>
                        <li><a href="${basepath}/manage/article">Manage Blog</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">TAG<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                    <#--        <li><a href="${basepath}/manage/tagIds/update">New Tag</a></li>-->
                        <li><a href="${basepath}/manage/tags">Manage Tag</a></li>
                    </ul>
                </li>
                <li><a href="#PROJECTS">PROJECTS</a></li>
                <li><a href="#ABOUT">ABOUT</a></li>
            </ul>
         </div>
     </div>
 </nav>
</#macro>