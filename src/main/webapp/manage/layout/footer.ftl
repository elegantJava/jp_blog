<#macro footer>
<div class="row foot-wrapper footer">
    <div class="col-lg-12">
        <hr>
        <p  id="foot" class="text-muted"> </p>
    </div>

</div>
<script type="text/javascript">
    $(function()
    {
        var mydate = new Date();
        $('#foot').html('&copy 2016-'+ + mydate.getFullYear() + " CopyRight  <a href='${basepath}/index'>pjzhong.com</a>");
    })
</script>
</#macro>