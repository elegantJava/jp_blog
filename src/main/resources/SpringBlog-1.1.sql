
Create database springblog;

use springblog;

Drop table  if exists article;
CREATE TABLE article
(
	id          int(11)         NOT NULL AUTO_INCREMENT,
    category_id int(11 )        NOT NULL COMMENT  '分类ID',
    title       varchar(40)     NOT NULL COMMENT '标题',
    content     varchar(20000)  NOT NULL COMMENT '内容',
    state      varchar(20)      NOT NULL COMMENT 'Published or Draft',
    author      varchar(15)     DEFAULT 'pj_zhong' COMMENT '作者',
	reading_count  int(11)      NOT NULL DEFAULT 0 COMMENT '阅读统计',
    create_time DATETIME        NOT NULL,
    update_time DATETIME        NOT NULL, 
	PRIMARY KEY(id),
    KEY atricle_category (category_id)
);

ALTER TABLE article MODIFY reading_count int(11) DEFAULT 0 COMMENT '阅读统计';


CREATE TABLE article_image (
  id        int(11)      NOT NULL AUTO_INCREMENT,
  articleId int(11)      NOT NULL COMMENT '文章Id',
  imageUrl  varchar(100) NOT NULL COMMENT '图片地址',
  PRIMARY KEY (`id`,`articleId`),
  KEY `rticle_image_id` (`articleId`)
) ENGINE=InnoDB AUTO_INCREMENT=190 DEFAULT CHARSET=utf8 COMMENT='文章图  主要用于列表浏览';

drop table categoryId;
CREATE TABLE categoryId
(
	id            int(11)       NOT NULL AUTO_INCREMENT,
    category_name varchar(20)   NOT NULL COMMENT '分类名 唯一',      
    alias_name    varchar(20)   NOT NULL COMMENT '别名 唯一',
    sort          int(11)       NOT NULL COMMENT '排序 (0-10)', 
	create_time DATETIME        NOT NULL,
    update_time DATETIME        NOT NULL, 
    PRIMARY KEY(id),
    UNIQUE  KEY aliasName_UNIQUE (alias_name),
    UNIQUE  KEY categoryName_UNIQUE (category_name)
);

DROP table tags;
CREATE TABLE tags (
  id      int(11)     NOT NULL AUTO_INCREMENT,
  name varchar(25) NOT NULL COMMENT '标签名称  唯一',
  create_time DATETIME        NOT NULL,
  update_time DATETIME        NOT NULL, 
  PRIMARY KEY (`id`),
  UNIQUE KEY `tagName_UNIQUE` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='标签表';

CREATE TABLE article_tag
(
	article_id int(11) NOT NULL,
    tag_id     int(11) NOT NULL,
    PRIMARY KEY (article_id,tag_id)
);

DROP table user;
CREATE TABLE users
(
	id        int(11)       NOT NULL AUTO_INCREMENT,
    email     varchar(40)   NOT NULL ,
    password  varchar(100)  NOT NULL ,
    create_time datetime,
    update_time datetime,
    PRIMARY KEY(id)
);

show tables;

desc article;

 select * from article a INNER JOIN article_tag ar_tag ON a.id = ar_tag.article_id  WHERE 1 and category_id = 1 and title like '%t%' group by a.id





select * from article where category_id = 1;



and tag_id in (36)

select * from article;

select * from article_tag;


