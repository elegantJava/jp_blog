package com.zjp.blog.client;

import com.zjp.blog.model.Category;

import java.util.List;

public interface CategoryMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Category record);

    int insertSelective(Category record);

    Category selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Category record);

    int updateByPrimaryKey(Category record);

    /*Personal extend*/
    Category selectByName(String name);

    List<Category> searchByName(String name);

    List<Category> selectAll();
}