package com.zjp.blog.client;

import com.zjp.blog.model.Tag;

import java.util.List;

public interface TagMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Tag record);

    int insertSelective(Tag record);

    Tag selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Tag record);

    int updateByPrimaryKey(Tag record);

    /* Personal extends */
    Tag selectByName(String name);

    List<Tag> searchByName(String name);

    List<Tag> selectByPostId(Integer id);

    List<Tag> selectByIds(List<Integer> tagIds);

    List<Tag> selectAll();

    int deleteByID(Integer id);
}