package com.zjp.blog.client;

import com.zjp.blog.model.Article;
import com.zjp.blog.model.Tag;
import com.zjp.blog.service.support.ArticleQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ArticleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Article record);

    int insertSelective(Article record);

    Article selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Article record);

    int updateByPrimaryKey(Article record);

    /** Personal extends */

    List<Article> search(ArticleQuery query);

    List<Article> selectAll();

    List<Article> selectByCategory(Integer id);

    List<Article> selectByTag(Integer id);

    int insertTagRelations(Article article);

    int deleteTagRelations(Integer id);

    int increaseReadingCount(Integer id);
}