package com.zjp.blog.service;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zjp.blog.client.ArticleMapper;
import com.zjp.blog.client.CategoryMapper;
import com.zjp.blog.client.TagMapper;
import com.zjp.blog.controller.PagerBean;
import com.zjp.blog.controller.error.NotFoundException;
import com.zjp.blog.model.Article;
import com.zjp.blog.model.Category;
import com.zjp.blog.model.Tag;
import com.zjp.blog.service.support.ArticleQuery;
import com.zjp.blog.utils.Validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author pj_zhong
 * @email pj_zhong@163.com / pj_zhong@gmail.com
 */

@Service
public class ArticleService
{
    @Autowired
    ArticleMapper articleMapper;

    @Autowired
    CategoryMapper categoryMapper;

    @Autowired
    TagMapper tagMapper;

    public PagerBean<Article> getPage(PagerBean<Article> pager,ArticleQuery query)
    {
        PageHelper.startPage(pager.getPageNum(), pager.getLength());
        List<Article> articles = search(query);

        PageInfo pageInfo = new PageInfo(articles);

        pager.setPages(pageInfo.getPages());
        pager.setRecords(articles);

        return pager;
    }

    public List<Article> search(ArticleQuery query)
    {
        if(query==null){
            query = new ArticleQuery();

        }else{
            if(!Validation.isEmpty(query.getTitle()) ){
                query.setTitle("%" + query.getTitle() + "%");
            }else{
                query.setTitle(null);
            }

            if(Validation.isEmpty(query.getState())){
                query.setState(null);
            }

            if(query.getTagIds()!=null && query.getTagIds().isEmpty()){
                query.setTagIds(null);
            }


        }

        List<Article> articles = articleMapper.search(query);

        for (Article a : articles) {
            a.setTags(tagMapper.selectByPostId(a.getId()));
            a.setCategory(categoryMapper.selectByPrimaryKey(a.getCategoryId()));
        }

        return articles;
    }

    public Article getById(Integer id)
    {
        Article article = articleMapper.selectByPrimaryKey(id);

        if(article==null) {
            throw new NotFoundException("Article:" + id + "Not Found");
        }

        Category category = categoryMapper.selectByPrimaryKey(article.getCategoryId());
        List<Tag> tags = tagMapper.selectByPostId(article.getId());
        article.setTags(tags);
        article.setCategory(category);

        return article;
    }

    @Transactional
    public Article create(Article article)
    {
        article.setUpdateTime(new Date());

        article.setCreateTime(article.getUpdateTime());
        articleMapper.insert(article);
        if( article.getTags() != null && !article.getTags().isEmpty() ) {
            articleMapper.insertTagRelations(article);
        }
        return article;
    }

    public Article update(Article article)
    {
        article.setUpdateTime(new Date());

        articleMapper.updateByPrimaryKeySelective(article);
        articleMapper.deleteTagRelations(article.getId());
        if( article.getTags() != null && !article.getTags().isEmpty() ) {
            articleMapper.insertTagRelations(article);
        }

        return article;
    }

    public int delete(Integer id)
    {
        int count=0;
        count += articleMapper.deleteByPrimaryKey(id);
        count += articleMapper.deleteTagRelations(id);
        return count;
    }

}
