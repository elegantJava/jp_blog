package com.zjp.blog.service.support;

import java.util.ArrayList;
import java.util.List;

/**
 * @author pj_zhong
 * @email pj_zhong@163.com / pj_zhong@gmail.com
 */

/*
*
* */
public class ArticleQuery
{
    private String title;
    private String state;
    private List<Integer> tagIds;
    private Integer categoryId;

    public ArticleQuery()
    {
        title = null;
        tagIds = null;
        categoryId = -1;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public List<Integer> getTagIds() {
        return tagIds;
    }

    public void setTagIds(List<Integer> tagIds) {
        this.tagIds = tagIds;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void addTagId(Integer id) {
        if(id > 0){

            if(tagIds==null){
                tagIds = new ArrayList<Integer>();
            }

            tagIds.add(id);
        }
    }

    @Override
    public String toString() {
        return "ArticleQuery{" +
                "title='" + title + '\'' +
                ", state='" + state + '\'' +
                ", tagIds=" + tagIds +
                ", categoryId=" + categoryId +
                '}';
    }
}
