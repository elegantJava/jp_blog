package com.zjp.blog.service;

import com.zjp.blog.client.UserMapper;
import com.zjp.blog.model.User;
import com.zjp.blog.utils.MD5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author pj_zhong
 * @email pj_zhong@163.com / pj_zhong@gmail.com
 */

@Service
public class UserService
{
    @Autowired
    private UserMapper mapper;

   public User selectByEmail(String email) {
        return mapper.selectByEmail(email);
    }


   public User createUser(User user) {
        user.setPassword(MD5.md5(user.getPassword()));
        mapper.insert(user);
        return  user;
    }

    public int  updateById(User user)
    {
       return  mapper.updateByPrimaryKeySelective(user);
    }

}
