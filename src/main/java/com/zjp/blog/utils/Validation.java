package com.zjp.blog.utils;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author pj_zhong
 * @email pj_zhong@163.com / pj_zhong@gmail.com
 */

public class Validation
{
    private static Matcher EmailAddress = Pattern.compile("\\w+@\\w{2,6}\\.\\w{2,3}+").matcher("");
    private static Matcher numberOnly = Pattern.compile("\\d+").matcher("");
    private static Matcher titleMatcher = Pattern.compile("[\\w\\-]{5,20}+").matcher("");
    private static Matcher specialSymbol = Pattern.compile("[\\W]*").matcher("");

    public static boolean EmailVerify(String email) {
        return EmailAddress.reset(email).matches();
    }

    public static boolean TitleVerify(String title) {
        return titleMatcher.reset(title).matches();
    }

    public static boolean isHasSpecialSymbol(String text) {
        return specialSymbol.reset(text).matches();
    }

    public static boolean isNumberOnly(String text) {
       return  numberOnly.reset(text).matches();
    }


    public static void main(String[] args) {
        String title ="asldjflka";

        System.out.println();
    }

    public static boolean isEmpty(String text) {
        if(text==null ||  "".equals(text)) {
            return true;
        }
        return false;
    }
}
