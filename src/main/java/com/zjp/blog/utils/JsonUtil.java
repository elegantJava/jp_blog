package com.zjp.blog.utils;

import com.google.gson.Gson;

/**
 * @author pj_zhong
 * @email pj_zhong@163.com / pj_zhong@gmail.com
 */

public class JsonUtil {
    private static Gson gson = new Gson();
    public static Gson getGson(){
        return  gson;
    }

    public static <T> T fromJson(String json,Class<T> object){
            return getGson().fromJson(json,object);
    }
}
