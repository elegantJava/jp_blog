package com.zjp.blog.aspect;

import com.zjp.blog.utils.Validation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @author pj_zhong
 * @email pj_zhong@163.com / pj_zhong@gmail.com
 */

@Aspect
@Component
public class ExceptionLogger{

    private static final Logger logger = LogManager.getLogger();

    @AfterThrowing(pointcut = "execution(* com.zjp.blog.service..*(..))",throwing = "ex")
    public void loggingExceptions(JoinPoint joinPoint, Exception ex){
        StringBuilder errorMessage = new StringBuilder();

        errorMessage.append(joinPoint.getTarget().getClass().getName());
        errorMessage.append(" Method:");
        errorMessage.append(joinPoint.getSignature().getName());
        errorMessage.append(" ");
        errorMessage.append(ex);

        logger.error(errorMessage);
    }
}
