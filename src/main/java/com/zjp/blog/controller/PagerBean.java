package com.zjp.blog.controller;

import java.util.List;

/**
 * @author pj_zhong
 * @email pj_zhong@163.com / pj_zhong@gmail.com
 */

public class PagerBean<T>
{
    private int pageNum=1;

    private int pages;

    private int length=10;

    private List<T> records;

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return "PagerBean{" +
                "pageNum=" + pageNum +
                ", pages=" + pages +
                ", records=" + records +
                '}';
    }
}
