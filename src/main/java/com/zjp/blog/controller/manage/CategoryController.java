package com.zjp.blog.controller.manage;

import com.zjp.blog.controller.BaseController;
import com.zjp.blog.controller.PagerBean;
import com.zjp.blog.controller.error.NotFoundException;
import com.zjp.blog.model.Category;
import com.zjp.blog.service.CategoryService;
import com.zjp.blog.utils.Validation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @author pj_zhong
 * @email pj_zhong@163.com / pj_zhong@gmail.com
 */

@Controller("")
@RequestMapping("/manage/category")
public class CategoryController extends BaseController
{
    private static final Logger logger = LogManager.getLogger(CategoryController.class);

    /*Url*/
    public static String tohome = "/manage/category";

    /*View*/
    public static String categoryHome = "/manage/category/Home";
    public static String categoryEdit = "/manage/category/editOrCreate";
    public static String categoryData = "/manage/category/data";

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(method = RequestMethod.GET)
    public String home(PagerBean<Category> pager, ModelMap map)
    {
        return categoryHome;
    }

    @RequestMapping(value = "load",method = RequestMethod.GET)
    public String load(String keyword,PagerBean<Category> pager, ModelMap map)
    {
        if(!Validation.isEmpty(keyword)) {
            logger.info("Searching:"+keyword);
            pager = categoryService.getPage(keyword,pager);
        }else {
            pager = categoryService.getPage(pager);
        }

        map.addAttribute("items",pager.getRecords());
        map.addAttribute("pager",pager);

        return categoryData;
    }


    @RequestMapping(value = "update",method = RequestMethod.GET)
    public String write(String id, ModelMap map)
    {
        if(!Validation.isEmpty(id)) {
            Category category = categoryService.getById(Integer.parseInt(id));
            if (category == null) throw new NotFoundException("Tag " + category.getId() + "Not found");

            map.addAttribute("item",category);
        }

        return categoryEdit;
    }

    @RequestMapping(value = "update",method = RequestMethod.POST)
    public String write(Category category, RedirectAttributes redirectAttributes)
    {
        if(isHasError(category)) {
            redirectAttributes.addAttribute("item",category);
            return categoryEdit;
        }

        if (category.getId() != null && category.getId() > 0) {
            logger.info("Update:" + category);
            addMessage(redirectAttributes, "Update Success");
            categoryService.update(category);
        } else {
            logger.info("Create:" + category);
            categoryService.create(category);
            addMessage(redirectAttributes, "Create Success");
        }

        return "redirect:" + tohome;
    }

    @RequestMapping(value = "delete",method = RequestMethod.POST)
    public String delete(@RequestParam("id")  String id, RedirectAttributes redirectAttributes)
    {
        if(!Validation.isEmpty(id)) {
            categoryService.deleteById(Integer.parseInt(id));
            addMessage(redirectAttributes,"Delete Success");
        }

        return "redirect:" + tohome;
    }

    public boolean isHasError(Category category)
    {
        if(category == null || Validation.isEmpty(category.getCategoryName())) {
            return true;
        }

        return false;
    }
}
