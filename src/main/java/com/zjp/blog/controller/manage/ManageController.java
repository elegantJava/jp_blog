package com.zjp.blog.controller.manage;

import com.zjp.blog.model.User;
import com.zjp.blog.utils.MD5;
import com.zjp.blog.controller.BaseController;
import com.zjp.blog.service.UserService;
import com.zjp.blog.utils.Validation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

/**
 * @author pj_zhong
 * @email pj_zhong@163.com / pj_zhong@gmail.com
 */
@Controller
@RequestMapping("/manage")
public class ManageController extends BaseController
{
    private static final Logger logger = LogManager.getLogger(ManageController.class);

    public  static String to_login="/manage/login";
    public  static String page_login="/manage/admin/login";
    public  static String manage_home="/manage/admin/home";

   @Autowired
    UserService service;

    @RequestMapping(method = RequestMethod.GET)
    public String home()
    {
        return manage_home;
    }

    @RequestMapping(value = {"login","tologin"},method = RequestMethod.GET)
    public String toLogin()
    {
        return page_login;
    }

    @RequestMapping(value= "login",method = RequestMethod.POST)
    public String login(@RequestParam(value = "userEmail",defaultValue = "") String username,
                        @RequestParam(value = "password",defaultValue = "") String password,
                        ModelMap map) throws Exception {

        if(Validation.isEmpty(username)|| Validation.isEmpty(password)) {
            map.addAttribute("errorMsg","Account and Password Can't not be blank");
            return page_login;
        }

        User user = service.selectByEmail(username);
        if(user ==null || !MD5.md5(password).equals(user.getPassword())) {
            addError(map,"Account or Password Error");
            return page_login;
        }

        session.setAttribute("user_info",user);
        logger.info("Login:" + user + " " + new Date());
        return "redirect:" + ArticleController.getToHome();
    }

    @RequestMapping("logout")
    public String logout()
    {
        session.setAttribute("user_info",null);
        session.invalidate();

        return  "redirect:"+ to_login;
    }
}
