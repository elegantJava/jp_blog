package com.zjp.blog.controller.filters;

import com.zjp.blog.controller.manage.ManageController;
import org.apache.logging.log4j.LogManager;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author pj_zhong
 * @email pj_zhong@163.com / pj_zhong@gmail.com
 */

public class Security implements Filter
{

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(Security.class);

    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        HttpServletRequest servletRequest  = (HttpServletRequest)   request;
        HttpServletResponse servletResponse = (HttpServletResponse) response;
        String path = servletRequest.getContextPath();

        if(servletRequest.getRequestURI().indexOf("login") == -1 && servletRequest.getSession().getAttribute("user_info")==null) {
            servletResponse.sendRedirect(path + ManageController.to_login);
            return;
        }

        chain.doFilter(request,response);
    }

    public void destroy() {

    }


}
