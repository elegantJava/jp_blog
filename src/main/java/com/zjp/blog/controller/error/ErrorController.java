package com.zjp.blog.controller.error;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.ServletException;

/**
 * @author pj_zhong
 * @email pj_zhong@163.com / pj_zhong@gmail.com
 */

@Controller
@RequestMapping("error")
public class ErrorController {

    String  notFound  = "/error/404";
    String  internalError = "/error/500";

    @RequestMapping("404")
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String NotFound(ModelMap map){
        return notFound;
    }

    @RequestMapping(value= {"500","505"})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String InteralError(ModelMap map){
        return  internalError;
    }
}
