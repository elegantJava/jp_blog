package com.zjp.blog.controller.front;


import com.zjp.blog.controller.BaseController;
import com.zjp.blog.controller.PagerBean;
import com.zjp.blog.model.Article;
import com.zjp.blog.model.Category;
import com.zjp.blog.model.Tag;
import com.zjp.blog.model.support.ArticleStates;
import com.zjp.blog.service.ArticleService;
import com.zjp.blog.service.CategoryService;
import com.zjp.blog.service.TagService;
import com.zjp.blog.service.UserService;
import com.zjp.blog.service.support.ArticleQuery;
import com.zjp.blog.utils.JsonUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


/**
 * @author pj_zhong
 * @email pj_zhong@163.com / pj_zhong@gmail.com
 */

@Controller
@RequestMapping(value = {"/", "/index"})
public class MainController extends BaseController
{
    private static final Logger logger = LogManager.getLogger(MainController.class);

    private static String index = "/front/index";
    private static String data  = "/front/data";
    private static String article_page = "/front/article";
    private static String about = "/front/about";

    @Autowired
    UserService service;

    @Autowired
    ArticleService articleService;

    @Autowired
    TagService tagService;

    @Autowired
    CategoryService categoryService;

    @RequestMapping(method = RequestMethod.GET)
    public String index( ModelMap map)
    {
        List<Tag> tags = tagService.getAll();
        List<Category> categories = categoryService.getAll();

        map.addAttribute("tags",tags);
        map.addAttribute("categories",categories);

        return index;
    }

    @RequestMapping(value = "load",method = RequestMethod.POST)
    public String load(@RequestParam(value = "param",defaultValue = "",required = false) String param, PagerBean<Article> pager, ModelMap map)
    {
        ArticleQuery query = JsonUtil.fromJson(param,ArticleQuery.class);
        if(query==null){
            query = new ArticleQuery();
        }
        query.setState(ArticleStates.PUBLISHED.toString());
        pager.setLength(5);
        pager = articleService.getPage(pager,query);

        List<Article> articles;
        List<Tag>     tags = null;
        Category      category = null;

        if(query.getCategoryId()!=null && query.getCategoryId()!= -1){
            category = categoryService.getById(query.getCategoryId());
        }
        articles = pager.getRecords().isEmpty() ? null : pager.getRecords();
            tags = tagService.getByIds(query.getTagIds());

        map.addAttribute("pager",pager);
        map.addAttribute("category",category);
        map.addAttribute("tags",tags);
        map.addAttribute("articles",articles);

        return data;
    }

    @RequestMapping("/article/{id}")
    public String showArticle(@PathVariable  Integer id,ModelMap map){
        Article article = articleService.getById(id);
        map.addAttribute("article",article);

        return article_page;
    }

    @RequestMapping("about")
    public String aboutbe(ModelMap map)
    {
        return  about;
    }


}
