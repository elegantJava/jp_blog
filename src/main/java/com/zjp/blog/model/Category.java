package com.zjp.blog.model;

import java.io.Serializable;

public class Category extends BaseModel implements Serializable {
    /** 分类名 唯一 */
    private String categoryName;

    /** 排序 (0-10) */
    private Integer sort;

    private static final long serialVersionUID = 1L;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName == null ? null : categoryName.trim();
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }


    public boolean equals(Object e)
    {
        if(e instanceof Category) {
            Category other = (Category) e;

            if(other.getId() == this.getId()){
                return true;
            }
        }

        return false;
    }

    @Override
    public String toString() {
        return "Category{" +
                "categoryName='" + categoryName + '\'' +
                ", sort=" + sort + " " + super.toString() +
                '}';
    }
}