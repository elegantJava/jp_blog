package com.zjp.blog.model.support;

/**
 * @author Raysmond<i@raysmond.com>
 */
public enum ArticleStates {
    DRAFT("Draft"),
    PUBLISHED("Published");

    private String name;

    ArticleStates(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return name();
    }


    @Override
    public String toString() {
        return getName();
    }

    public static void main(String[] args)
    {
        for(ArticleStates s : ArticleStates.values())
        {
            System.out.println(s);
        }
    }
}
