package com.zjp.blog.model;

import com.zjp.blog.model.support.ArticleConstant;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

public class Article extends BaseModel implements Serializable {
    /** article.category_id */
    private Integer categoryId;

    /** 标题 */
    private String title;

    /** 内容 */
    private String content;

    /** Published or Draft */
    private String state;

    /** article.author */
    private String author;

    /** article.reading_count */
    private Integer readingCount;

    List<Tag> tags;

    private Category category;

    public String getPreview()
    {
        return ArticleConstant.PreviewLength < content.length() ? content.substring(0,ArticleConstant.PreviewLength) : content;
    }

    private static final long serialVersionUID = 1L;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author == null ? null : author.trim();
    }

    public Integer getReadingCount() {
        return readingCount;
    }

    public void setReadingCount(Integer readingCount) {
        this.readingCount = readingCount;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public String getCategoryName() {
        return category !=null ? category.getCategoryName() : "";
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Article{" +
                "categoryId=" + categoryId +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", state='" + state + '\'' +
                ", author='" + author + '\'' +
                ", readingCount=" + readingCount +
                ", tags=" + tags +
                '}';
    }
}