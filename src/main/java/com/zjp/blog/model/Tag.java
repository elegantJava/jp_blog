package com.zjp.blog.model;

import java.io.Serializable;

public class Tag extends BaseModel implements Serializable {
    /** 标签名称  唯一 */
    private String name;

    private static final long serialVersionUID = 1L;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

   @Override
    public boolean equals(Object o)
    {
        if(!(o instanceof  Tag)){
            return false;
        }

        Tag other = (Tag)o;
        return ( this.getId() == other.getId() ) && (this.getName().equals(other.getName()) );
    }

    @Override
    public String toString() {
        return "Tag{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public int hashCode(){
        int result = 17;
        result = 31 * result + name.hashCode();
        result = 31 * result + id;

        return result;
    }

    public static void main(String args[])
    {
/*        Tag t = new Tag();

        t.setName("asdf");
        t.setId(1);

        Tag b = new Tag();

        b.setName("asdasdff");
        b.setId(1);

        Tag c = new Tag();

        c.setName("asdasdfasdff");
        c.setId(2);


        System.out.println(t.equals(b) + " "  + b.equals(t));
        System.out.println(t.hashCode());
        System.out.println(b.hashCode());

        System.out.println(t.equals(c) + " "  + b.equals(c));
        System.out.println(c.hashCode());*/
    }
}