package com.zjp.blog.model;

import javax.xml.crypto.Data;
import java.util.Date;

/**
 * @author pj_zhong
 * @email pj_zhong@163.com / pj_zhong@gmail.com
 */

public class BaseModel
{
    Integer id;

    Date updateTime;

    Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "BaseModel{" +
                "id=" + id +
                ", updateTime=" + updateTime +
                ", createTime=" + createTime +
                '}';
    }
}
